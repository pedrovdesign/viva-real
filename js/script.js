$('#search').click(function(){

  var searchField = $('#search').val();
  var myExp = new RegExp(searchField, "i");
}); //get JSON

var dados = [];

$(document).ready(function(){
  $('#pagination').pagination({
      dataSource: 'sources/data.json',
      locator: 'items',
      pageSize: 20,
      totalNumber: 400,
      formatResult: function(data) {
        console.log(data.length)
      },
      callback: function(data, pagination) {
        dados = data;
        var itemsToRender = [];
        var currentItem = (pagination.pageNumber - 1) * pagination.pageSize;
        for (var i = 0; i < pagination.pageSize; i++) {
          var index = currentItem + i;
          var item = data[currentItem + i];
          //ßßßßconsole.log(pagination.pageNumber, currentItem + i);
          item.index = index;
          itemsToRender.push(item);
        }
        console.log(itemsToRender);
        renderPaginaPrincipal(itemsToRender);
      }
  });
});

function renderDetail(el) {
  var id = el.getAttribute("data-item");
  //console.log();
  var result = dados[id];
  console.log(result);
  const template = document.querySelector('#details').text;
  document.querySelector('#update').innerHTML = doT.template(template)({ result });

}

function renderPaginaPrincipal(results) {
  const template = document.querySelector('#template').text;
  document.querySelector('#update').innerHTML = doT.template(template)({ results });

}
